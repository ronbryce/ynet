<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Profile;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
	
	public function createProfile(Request $request){
		  $destinationPath = 'avatars'; // upload path
        $extension = $request->file('avatar')->getClientOriginalExtension(); // getting image extension
        $fileName = str_random(10) . '.' . $extension; // renaming image
        $request->file('avatar')->move($destinationPath, $fileName); // uploading file to given path
		$profile = new Profile($request->only(['fname', 'lname', 'tel', 'email', 'sex', 'dob', 'country',
						'currency', 'address']);
		$profile->image = $fileName;
		$profile->save();
		return respons
		
	}
}
