<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model{
	protected $fillable = ['fname', 'lname', 'tel', 'email', 'sex', 'dob', 'country', 'currency', 'address', 'image'];

}